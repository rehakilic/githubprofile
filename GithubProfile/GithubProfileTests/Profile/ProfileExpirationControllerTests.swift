//
//  ProfileExpirationControllerTests.swift
//  GithubProfileTests
//
//  Created by Reha Kılıç on 25.12.2021.
//

import XCTest
@testable import GithubProfile
@testable import Apollo

class ProfileExpirationControllerTests: XCTestCase {
    var sut: ProfileExpirationController!
    var userDefaultsController: UserDefaultsController!
    
    override func setUp() {
        super.setUp()
        let userDefaults = UserDefaults(suiteName: #file)!
        userDefaults.removePersistentDomain(forName: #file)
        userDefaultsController = UserDefaultsController(userDefaults)
        sut = ProfileExpirationController(userDefaultsController: userDefaultsController)
    }
    
    override func tearDown() {
        sut = nil
        userDefaultsController = nil
        super.tearDown()
    }
    
    func test_ShouldReturnIsExpiredIf24hPassed() {
        // Given
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.hour.rawValue
        let nowString = dateFormatter.string(from: Date())
        let now = dateFormatter.date(from: nowString)!
        let expireDate = Calendar.current.date(byAdding: .minute, value: -(25 * 60), to: now)!
        let lastFetchDate = dateFormatter.string(from: expireDate)
        userDefaultsController.lastFetchDate = lastFetchDate
        
        // When
        let isExpired = sut.isCacheExpired
        
        // Then
        XCTAssertTrue(isExpired)
    }
    
    func test_ShouldReturnIsExpiredIfLastFetchTimeNil() {
        // Given
        userDefaultsController.lastFetchDate = nil
        
        // When
        let isExpired = sut.isCacheExpired
        
        // Then
        XCTAssertTrue(isExpired)
    }
    
    func test_ShouldReturnIsExpiredIfLastFetchWrongFormat() {
        // Given
        userDefaultsController.lastFetchDate = "15-12-3"
        
        // When
        let isExpired = sut.isCacheExpired
        
        // Then
        XCTAssertTrue(isExpired)
    }
    
    func test_ShouldClearLastFetchDateIfExpired() {
        // Given
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.hour.rawValue
        let nowString = dateFormatter.string(from: Date())
        let now = dateFormatter.date(from: nowString)!
        let expireDate = Calendar.current.date(byAdding: .minute, value: -(24 * 61), to: now)!
        let lastFetchDate = dateFormatter.string(from: expireDate)
        userDefaultsController.lastFetchDate = lastFetchDate
        
        // When
        let isExpired = sut.isCacheExpired
        
        // Then
        XCTAssertTrue(isExpired)
        XCTAssertNil(userDefaultsController.lastFetchDate)
    }
    
    func test_ShouldReturnNotExpiredIf24hNotPassed() {
        // Given
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.hour.rawValue
        let nowString = dateFormatter.string(from: Date())
        let now = dateFormatter.date(from: nowString)!
        let expireDate = Calendar.current.date(byAdding: .minute, value: -(23 * 59), to: now)!
        let lastFetchDate = dateFormatter.string(from: expireDate)
        userDefaultsController.lastFetchDate = lastFetchDate
        
        // When
        let isExpired = sut.isCacheExpired
        
        // Then
        XCTAssertFalse(isExpired)
    }
    
    func test_ShouldSaveCacheTime() {
        // Given
        userDefaultsController.lastFetchDate = nil
        
        // When
        sut.saveCacheTime()
        
        // Then
        XCTAssertNotNil(userDefaultsController.lastFetchDate)
    }
    
    func test_ShouldNotSaveCacheTimeIfLastFetchNotNil() {
        // Given
        let lastFetchDate = "Test"
        userDefaultsController.lastFetchDate = lastFetchDate
        
        // When
        sut.saveCacheTime()
        
        // Then
        XCTAssertEqual(userDefaultsController.lastFetchDate, lastFetchDate)
    }
}
