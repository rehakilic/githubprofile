//
//  ProfilePresenterTests.swift
//  GithubProfileTests
//
//  Created by Reha Kılıç on 24.12.2021.
//


import XCTest
@testable import GithubProfile
@testable import Apollo

class ProfilePresenterTests: XCTestCase {
    var sut: ProfilePresenter!
    var expirationController: MockExpirationController!
    var profileView: MockProfileView!
    
    override func setUp() {
        super.setUp()
        let apolloNetworkController = ApolloNetworkController(networkTransport: MockNetworkTransport(),
                                                              store: ApolloStore(cache: InMemoryNormalizedCache()))
        expirationController = MockExpirationController()
        sut = ProfilePresenter(apolloNetworkController: apolloNetworkController,
                               expirationController: expirationController)
        profileView = MockProfileView()
        sut.profileView = profileView
    }
    
    override func tearDown() {
        sut = nil
        expirationController = nil
        profileView = nil
        super.tearDown()
    }
    
    
    func test_ShouldAddContentWhenFetchedProfile() {
        // When
        sut.didLoad()
        
        // Then
        XCTAssertNotNil(profileView.addedContent)
        XCTAssertTrue(expirationController.saveCacheTimeCalled)
        XCTAssertNil(profileView.errorMessage)
    }
    
    func test_ShouldNotAddContentWhenFetchedProfileFailed() {
        // Given
        let networkTransport = MockNetworkTransport()
        networkTransport.status = .failure
        sut = ProfilePresenter(apolloNetworkController: ApolloNetworkController(networkTransport: networkTransport,
                                                                                store: ApolloStore(cache: InMemoryNormalizedCache())),
                               expirationController: MockExpirationController())
        sut.profileView = profileView
        
        // When
        sut.didLoad()
        
        // Then
        XCTAssertNotNil(profileView.errorMessage)
        XCTAssertNil(profileView.addedContent)
        XCTAssertFalse(expirationController.saveCacheTimeCalled)
    }
    
    func test_ShouldClearCacheAndFetchIfCacheExpired() {
        // Given
        let cacheKey = CacheKey("T")
        let cache = InMemoryNormalizedCache(records: RecordSet(records: [Record(key: cacheKey)]))
        let apolloNetworkController = ApolloNetworkController(networkTransport: MockNetworkTransport(),
                                                              store: ApolloStore(cache: cache))
        expirationController.isExpired = true
        sut = ProfilePresenter(apolloNetworkController: apolloNetworkController,
                               expirationController: expirationController)
        sut.profileView = profileView
        
        // When
        sut.didLoad()
        
        // Then
        var cacheSet = Set<CacheKey>()
        cacheSet.insert(cacheKey)
        let records = try! cache.loadRecords(forKeys: cacheSet)
        XCTAssertTrue(records.isEmpty)
        
        XCTAssertTrue(expirationController.saveCacheTimeCalled)
        XCTAssertNotNil(profileView.addedContent)
        XCTAssertNil(profileView.errorMessage)
        
    }
}
