//
//  Mocks.swift
//  GithubProfileTests
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
@testable import GithubProfile
@testable import Apollo

class MockExpirationController: ExpirationController {
    var isExpired = false
    var saveCacheTimeCalled = false
    
    var isCacheExpired: Bool {
        return isExpired
    }
    
    func saveCacheTime() {
        saveCacheTimeCalled = true
    }
}

enum NetworkStatus {
    case success, failure
}

enum NetworkError: Error {
    case failed
}

class MockNetworkTransport: NetworkTransport {
    var status: NetworkStatus = .success
    
    func send<Operation>(operation: Operation, cachePolicy: CachePolicy, contextIdentifier: UUID?, callbackQueue: DispatchQueue, completionHandler: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) -> Cancellable where Operation : GraphQLOperation {

        let result = GraphQLResult<Operation.Data>(data: mockFetchQueryData as? Operation.Data, extensions: nil, errors: nil, source: .server, dependentKeys: nil)
        
        switch self.status {
        case .success:
            completionHandler(.success(result))
        case .failure:
            completionHandler(.failure(NetworkError.failed))
        }
        return MockTask()
    }
    
    private final class MockTask: Cancellable {
        func cancel() { }
    }
}

class MockProfileView: ProfileViewProtocol {
    private(set) var addedContent: Profile?
    private(set) var errorMessage: String?
    private(set) var showLoadingCalled = false
    private(set) var removeLoadingCalled = false
    
    func addContent(for profile: Profile) {
        addedContent = profile
    }
    
    func errorOccured(_ error: String) {
        errorMessage = error
    }
    
    func showLoading() {
        showLoadingCalled = true
    }
    
    func removeLoading() {
        removeLoadingCalled = true
    }
}




let mockFetchQueryData = FetchQuery.Data(viewer: FetchQuery.Data.Viewer(name: "Reha",
                                                                        login: "rehakilicdev",
                                                                        avatarUrl: "https://avatars.githubusercontent.com/u/9960162?u=135d9e26e993f260fe2cbe3550ec96c3f4ba65a2&v=4", email: "rehakilic@yandex.com",
                                                                        followers: FetchQuery.Data.Viewer.Follower(totalCount: 15),
                                                                        following: FetchQuery.Data.Viewer.Following(totalCount: 25),
                                                                        topRepositories: FetchQuery.Data.Viewer.TopRepository(nodes: [FetchQuery.Data.Viewer.TopRepository.Node(name: "testRepo", stargazerCount: 15, description: "Test Desc", primaryLanguage: FetchQuery.Data.Viewer.TopRepository.Node.PrimaryLanguage(name: "Swift", color: "#FFFFFF"), owner: FetchQuery.Data.Viewer.TopRepository.Node.Owner.makeUser(avatarUrl: "https://avatars.githubusercontent.com/u/9960162?u=135d9e26e993f260fe2cbe3550ec96c3f4ba65a2&v=4", login: "rehakilic"))]),
                                                                        starredRepositories: FetchQuery.Data.Viewer.StarredRepository(nodes: [FetchQuery.Data.Viewer.StarredRepository.Node(name: "test starred", stargazerCount: 13, description: "starred desc", primaryLanguage: FetchQuery.Data.Viewer.StarredRepository.Node.PrimaryLanguage(name: "swift", color: "#FFFFFF"), owner: FetchQuery.Data.Viewer.StarredRepository.Node.Owner.makeUser(avatarUrl: "https://avatars.githubusercontent.com/u/9960162?u=135d9e26e993f260fe2cbe3550ec96c3f4ba65a2&v=4", login: "rehakilic"))]),
                                                                        pinnedItems: FetchQuery.Data.Viewer.PinnedItem(edges: [FetchQuery.Data.Viewer.PinnedItem.Edge(node: FetchQuery.Data.Viewer.PinnedItem.Edge.Node.makeRepository(name: "Pinned", stargazerCount: 15, description: "Pinned desc", primaryLanguage: FetchQuery.Data.Viewer.PinnedItem.Edge.Node.AsRepository.PrimaryLanguage(name: "swift", color: "#FFFFFF"), owner: FetchQuery.Data.Viewer.PinnedItem.Edge.Node.AsRepository.Owner.makeUser(avatarUrl: "https://avatars.githubusercontent.com/u/9960162?u=135d9e26e993f260fe2cbe3550ec96c3f4ba65a2&v=4", login: "rehakilic")))])))
