//
//  DimensionConstant.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

enum DimensionConstant {
    enum Spacing {
        static let xsmall: CGFloat = 8
        static let small: CGFloat = 12
        static let medium: CGFloat = 16
        static let large: CGFloat = 20
    }
}
