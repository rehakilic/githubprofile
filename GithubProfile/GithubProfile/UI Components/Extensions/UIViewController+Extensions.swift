//
//  UIViewController+Extensions.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

extension UIViewController {
    func add(_ child: UIViewController, to containerView: UIView) {
        addChild(child)
        containerView.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.topAnchor.constraint(equalTo: view.topAnchor),
            child.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            child.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            child.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func add(_ child: UIViewController, to stackView: UIStackView) {
        addChild(child)
        child.didMove(toParent: self)
        stackView.addArrangedSubview(child.view)
    }

    func remove() {
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}

