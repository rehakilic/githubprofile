//
//  Fontbook.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

enum FontBook: String, CaseIterable {
    case bold = "SourceSansPro-Bold"
    case regular = "SourceSansPro-Regular"
    case semibold = "SourceSansPro-SemiBold"

    func withStyle(of style: UIFont.TextStyle) -> UIFont {
        let preferred =  UIFont.preferredFont(forTextStyle: style).pointSize
        guard let font =  UIFont(name: self.rawValue, size: preferred) else {
            assertionFailure("Font not found!")
            return UIFont.systemFont(ofSize: preferred)
        }
        return font
    }
}

