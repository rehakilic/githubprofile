//
//  ImageDownloader.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import UIKit

protocol ImageDownloader {
    func loadImage(from url: URL, handler: @escaping (UIImage?, Error?) -> Void)
}

final class DefaultImageDownloader: ImageDownloader {
    private let session: URLSession
    private let imageCacher: ImageCacher

    init(session: URLSession = .shared, imageCacher: ImageCacher = ImageCacher()) {
        self.session = session
        self.imageCacher = imageCacher
    }

    func loadImage(from url: URL, handler: @escaping (UIImage?, Error?) -> Void) {
        if let image = imageCacher.item(for: url.absoluteString) {
            handler(image, nil)
            return
        }

        session.dataTask(with: url) { data, _, error in
            guard
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    DispatchQueue.main.async { handler(nil, error) }
                    return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.imageCacher.set(value: image, for: url.absoluteString)
                handler(image, nil)
            }
        }.resume()
    }
}
