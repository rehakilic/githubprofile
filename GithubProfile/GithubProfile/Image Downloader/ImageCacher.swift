//
//  ImageCacher.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

protocol Cache {
    associatedtype Object
    var items: [String: Object] { get }
    func set(value: Object, for key: String)
    func item(for key: String) -> Object?
}

final class ImageCacher: Cache {
    typealias Object = UIImage
    var items: [String: UIImage] = [:]

    init() {}

    func set(value: UIImage, for key: String) {
        items[key] = value
    }

    func item(for key: String) -> UIImage? {
        return items[key]
    }
}

