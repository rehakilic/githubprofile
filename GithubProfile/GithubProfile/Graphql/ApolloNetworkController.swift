//
//  ApolloNetworkController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 23.12.2021.
//

import Foundation
import Apollo
import ApolloSQLite

final class ApolloNetworkController {
    let apollo: ApolloClient
    
    init(networkTransport: NetworkTransport, store: ApolloStore) {
        self.apollo = ApolloClient(networkTransport: networkTransport, store: store)
    }
}

class UserManagementInterceptor: ApolloInterceptor {
    private static let token = "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"
    
    enum UserError: Error {
        case noUserLoggedIn
    }
    
    func interceptAsync<Operation: GraphQLOperation>(
        chain: RequestChain,
        request: HTTPRequest<Operation>,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
            
            self.addTokenAndProceed(UserManagementInterceptor.token,
                                    to: request,
                                    chain: chain,
                                    response: response,
                                    completion: completion)
        }
    
    /// Helper function to add the token then move on to the next step
    private func addTokenAndProceed<Operation: GraphQLOperation>(
        _ token: String,
        to request: HTTPRequest<Operation>,
        chain: RequestChain,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
            
            request.addHeader(name: "Authorization", value: "Bearer \(token)")
            chain.proceedAsync(request: request,
                               response: response,
                               completion: completion)
        }
}

struct NetworkInterceptorProvider: InterceptorProvider {
    // These properties will remain the same throughout the life of the `InterceptorProvider`, even though they
    // will be handed to different interceptors.
    private let store: ApolloStore
    private let client: URLSessionClient
    
    init(store: ApolloStore,
         client: URLSessionClient) {
        self.store = store
        self.client = client
    }
    
    func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
        return [
            MaxRetryInterceptor(),
            CacheReadInterceptor(store: self.store),
            UserManagementInterceptor(),
            NetworkFetchInterceptor(client: self.client),
            ResponseCodeInterceptor(),
            JSONResponseParsingInterceptor(cacheKeyForObject: self.store.cacheKeyForObject),
            AutomaticPersistedQueryInterceptor(),
            CacheWriteInterceptor(store: self.store)
        ]
    }
}

