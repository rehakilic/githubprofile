//
//  GraphqlMappings.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 23.12.2021.
//

extension FetchQuery.Data.Viewer {
    func createProfile() -> Profile {
        var sRepositories: [Repository] = []
        starredRepositories.nodes?.forEach({ node in
            guard let node = node else { return }
            let repository = Repository(name: node.name, starCount: node.stargazerCount, description: node.description, primaryLanguageName: node.primaryLanguage?.name, primaryLanguageColor: node.primaryLanguage?.color, ownerAvatarUrl: node.owner.avatarUrl, ownerName: node.owner.login)
            sRepositories.append(repository)
        })
        var pRepositories: [Repository] = []
        pinnedItems.edges?.forEach({ edge in
            guard let node = edge?.node?.asRepository else { return }
            
            let repository = Repository(name: node.name, starCount: node.stargazerCount, description: node.description, primaryLanguageName: node.primaryLanguage?.name, primaryLanguageColor: node.primaryLanguage?.color, ownerAvatarUrl: node.owner.avatarUrl, ownerName: node.owner.login)
            pRepositories.append(repository)
        })
        var tRepositories: [Repository] = []
        topRepositories.nodes?.forEach({ node in
            guard let node = node else { return }
            let repository = Repository(name: node.name, starCount: node.stargazerCount, description: node.description, primaryLanguageName: node.primaryLanguage?.name, primaryLanguageColor: node.primaryLanguage?.color, ownerAvatarUrl: node.owner.avatarUrl, ownerName: node.owner.login)
            tRepositories.append(repository)
        })
        
        
        return Profile(name: name, login: login, avatarUrl: avatarUrl, email: email, followerCount: followers.totalCount, followingCount: following.totalCount, starredRepositories: sRepositories, pinnedRepositories: pRepositories, topRepositories: tRepositories)
    }
}

struct Profile {
    let name: String?
    let login: String
    let avatarUrl: String
    let email: String
    let followerCount: Int
    let followingCount: Int
    let starredRepositories: [Repository]
    let pinnedRepositories: [Repository]
    let topRepositories: [Repository]
}

struct Repository {
    let name: String?
    let starCount: Int
    let description: String?
    let primaryLanguageName: String?
    let primaryLanguageColor: String?
    let ownerAvatarUrl: String
    let ownerName: String
}

