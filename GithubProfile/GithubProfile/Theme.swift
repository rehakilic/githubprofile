//
//  Theme.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

enum Theme {
    static func configure() {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black, .font: FontBook.bold.withStyle(of: .title3)]
        navBarAppearance.backgroundColor = .white
        UINavigationBar.appearance().standardAppearance = navBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
    }
}
