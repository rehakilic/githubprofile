//
//  UserDefaultsController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

final class UserDefaultsController {
    private let defaults: UserDefaults
    
    init(_ defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }
    
    var lastFetchDate: String? {
        get {
            return defaults.string(forKey: DefaultKey.lastFetchDate.rawValue)
        }
        set {
            defaults.set(newValue, forKey: DefaultKey.lastFetchDate.rawValue)
        }
    }
}

extension UserDefaultsController {
    enum DefaultKey: String {
        case lastFetchDate
    }
}
