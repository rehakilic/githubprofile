//
//  ProfileExpirationController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol ExpirationController {
    var isCacheExpired: Bool { get }
    func saveCacheTime()
}

final class ProfileExpirationController: ExpirationController {
    private let userDefaultsController: UserDefaultsController
    
    init(userDefaultsController: UserDefaultsController) {
        self.userDefaultsController = userDefaultsController
    }
    
    var isCacheExpired: Bool {
        guard let lastFetchTime = userDefaultsController.lastFetchDate else { return true }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.hour.rawValue
        guard let lastFetchDate = dateFormatter.date(from: lastFetchTime) else { return true }
        
        let nowString = dateFormatter.string(from: Date())
        guard let now = dateFormatter.date(from: nowString) else { return true }
        
        guard let expireDate = Calendar.current.date(byAdding: .minute, value: 24 * 60, to: lastFetchDate) else { return true }
        let isExpired = now > expireDate
        if isExpired {
            userDefaultsController.lastFetchDate = nil
        }
        return isExpired
    }
    
    func saveCacheTime() {
        guard userDefaultsController.lastFetchDate == nil else { return }
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.hour.rawValue
        userDefaultsController.lastFetchDate = dateFormatter.string(from: now)
    }
}

enum DateFormat: String {
    case hour = "yyyy-MM-dd HH:mm"
}
