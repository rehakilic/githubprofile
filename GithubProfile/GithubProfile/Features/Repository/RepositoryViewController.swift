//
//  RepositoryViewController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import UIKit

protocol RepositoryViewProtocol: AnyObject {
    func render(with model: RepositoryModel)
}

final class RepositoryViewController: UIViewController {
    private(set) lazy var repositoryView = RepositoryView()
    private let presenter: RepositoryPresenterProtocol
    private let dataSource: RepositoryDataSource
    private let collectionLayoutType: CollectionLayoutType
    
    init(presenter: RepositoryPresenterProtocol,
         dataSource: RepositoryDataSource,
         collectionLayoutType: CollectionLayoutType) {
        self.presenter = presenter
        self.dataSource = dataSource
        self.collectionLayoutType = collectionLayoutType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = repositoryView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        presenter.didLoad()
    }
}

extension RepositoryViewController: RepositoryViewProtocol {
    func render(with model: RepositoryModel) {
        repositoryView.titleLabel.text = model.title
    }
}

private extension RepositoryViewController {
    func configure() {
        repositoryView.collectionView.register(RepositoryCollectionViewCell.self, forCellWithReuseIdentifier: RepositoryCollectionViewCell.reuseIdentifier)
        repositoryView.collectionView.dataSource = dataSource
        repositoryView.collectionView.delegate = self
        if let layout = repositoryView.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = collectionLayoutType.direction
        }
    }
}

extension RepositoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionLayoutType {
        case .horizontal:
            return CGSize(width: 200, height: collectionView.frame.height)
        case .vertical:
            let count = CGFloat(dataSource.models.count)
            let spacings: CGFloat = (count - 1) * DimensionConstant.Spacing.medium
            let height = (collectionView.frame.height - spacings) / count
            return CGSize(width: collectionView.frame.width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        DimensionConstant.Spacing.medium
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        DimensionConstant.Spacing.medium
    }
}

enum CollectionLayoutType {
    case horizontal, vertical
    
    var direction: UICollectionView.ScrollDirection {
        switch self {
        case .horizontal:
            return .horizontal
        case .vertical:
            return .vertical
        }
    }
    
    var height: CGFloat {
        switch self {
        case .horizontal:
            return 200
        case .vertical:
            return 525
        }
    }
}
