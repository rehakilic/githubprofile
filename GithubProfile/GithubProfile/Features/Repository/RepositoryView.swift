//
//  RepositoryView.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

final class RepositoryView: UIView {
    lazy var titleLabel = makeTitleLabel()
    lazy var viewAllLabel = makeUnderlineLabel()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView.backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension RepositoryView {
    func setupView() {
        addSubview(titleLabel)
        addSubview(viewAllLabel)
        addSubview(collectionView)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let titleLabelConstraints = [
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: DimensionConstant.Spacing.medium)
        ]
        
        viewAllLabel.translatesAutoresizingMaskIntoConstraints = false
        let viewAllLabelConstraints = [
            viewAllLabel.topAnchor.constraint(equalTo: topAnchor),
            viewAllLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -DimensionConstant.Spacing.medium)
        ]
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let collectionViewConstraints = [
            collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: DimensionConstant.Spacing.medium),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: DimensionConstant.Spacing.medium),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -DimensionConstant.Spacing.medium),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(titleLabelConstraints)
        NSLayoutConstraint.activate(viewAllLabelConstraints)
        NSLayoutConstraint.activate(collectionViewConstraints)
    }
}

private extension RepositoryView {
    func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.font = FontBook.bold.withStyle(of: .title2)
        label.textColor = .black
        return label
    }
    
    func makeUnderlineLabel() -> UILabel {
        let label = UILabel()
        label.font = FontBook.semibold.withStyle(of: .callout)
        label.textColor = .black
        let text = "View All"
        let textRange = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        
        label.attributedText = attributedText
        return label
    }
}

