//
//  RepositoryCollectionViewCell.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import UIKit

final class RepositoryCollectionViewCell: UICollectionViewCell {
    lazy var avatarImageView = makeImageView()
    lazy var userNameLabel = makeLabel(font: .regular)
    lazy var reposioryNameLabel = makeLabel(font: .bold)
    lazy var reposioryDescriptionLabel = makeLabel(font: .regular)
    private lazy var starImageView = makeImageView()
    lazy var starCountLabel = makeLabel(font: .regular)
    lazy var languageColorView = UIView()
    lazy var languageLabel = makeLabel(font: .regular)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension RepositoryCollectionViewCell {
    func setupView() {
        let imageStackView = UIStackView(arrangedSubviews: [avatarImageView, userNameLabel])
        imageStackView.spacing = DimensionConstant.Spacing.xsmall
        imageStackView.alignment = .center
        
        let repositoryStackView = UIStackView(arrangedSubviews: [imageStackView, reposioryNameLabel, reposioryDescriptionLabel])
        repositoryStackView.axis = .vertical
        repositoryStackView.distribution = .fill
        
        let starStackView = UIStackView(arrangedSubviews: [starImageView, starCountLabel])
        starStackView.spacing = DimensionConstant.Spacing.xsmall
        
        let colorContainerView = UIView()
        colorContainerView.addSubview(languageColorView)
        let languageStackView = UIStackView(arrangedSubviews: [colorContainerView, languageLabel])
        languageStackView.spacing = DimensionConstant.Spacing.xsmall
        
        let starLanguageStackView = UIStackView(arrangedSubviews: [starStackView, languageStackView])
        starLanguageStackView.spacing = DimensionConstant.Spacing.medium
        
        let stackView = UIStackView(arrangedSubviews: [repositoryStackView, starLanguageStackView])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = DimensionConstant.Spacing.medium
        
        addSubview(stackView)
        
        let avatarImageSize: CGFloat = 32
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.layer.cornerRadius = avatarImageSize / 2
        let avatarImageViewConstraints = [
            avatarImageView.widthAnchor.constraint(equalToConstant: avatarImageSize),
            avatarImageView.heightAnchor.constraint(equalToConstant: avatarImageSize)
        ]
        
        let languageColorImageSize: CGFloat = 10
        languageColorView.translatesAutoresizingMaskIntoConstraints = false
        languageColorView.layer.cornerRadius = languageColorImageSize / 2
        let languageColorImageViewConstraints = [
            languageColorView.widthAnchor.constraint(equalToConstant: languageColorImageSize),
            languageColorView.heightAnchor.constraint(equalToConstant: languageColorImageSize),
            languageColorView.leadingAnchor.constraint(equalTo: colorContainerView.leadingAnchor),
            languageColorView.trailingAnchor.constraint(equalTo: colorContainerView.trailingAnchor),
            languageColorView.centerYAnchor.constraint(equalTo: colorContainerView.centerYAnchor)
        ]
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        let stackViewConstraints = [
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: DimensionConstant.Spacing.medium),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: DimensionConstant.Spacing.medium),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(avatarImageViewConstraints)
        NSLayoutConstraint.activate(languageColorImageViewConstraints)
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
    func configure() {
        avatarImageView.clipsToBounds = true
        starImageView.image = UIImage(systemName: "star.fill")?.withRenderingMode(.alwaysTemplate)
        starImageView.tintColor = .yellow
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
    }
}

private extension RepositoryCollectionViewCell {
    func makeLabel(font: FontBook, textStyle: UIFont.TextStyle = .callout) -> UILabel {
        let label = UILabel()
        label.font = font.withStyle(of: textStyle)
        label.textColor = .black
        return label
    }
    
    func makeImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
}
