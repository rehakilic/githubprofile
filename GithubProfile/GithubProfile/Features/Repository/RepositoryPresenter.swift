//
//  RepositoryPresenter.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol RepositoryPresenterProtocol {
    func viewAllTapped()
    func didLoad()
}

final class RepositoryPresenter: RepositoryPresenterProtocol {
    weak var repositoryView: RepositoryViewProtocol?
    private let model: RepositoryModel
    
    init(model: RepositoryModel) {
        self.model = model
    }
    
    func didLoad() {
        repositoryView?.render(with: model)
    }
    
    func viewAllTapped() {
        //
    }
}

struct RepositoryModel {
    let title: String
}

