//
//  RepositoryDataSource.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import UIKit

final class RepositoryDataSource: NSObject, UICollectionViewDataSource {
    private(set) var models: [Repository]
    private let imageDownloader: ImageDownloader
    
    init(models: [Repository], imageDownloader: ImageDownloader) {
        self.models = models
        self.imageDownloader = imageDownloader
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RepositoryCollectionViewCell.reuseIdentifier, for: indexPath) as? RepositoryCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let model = models[indexPath.item]
        
        cell.userNameLabel.text = model.ownerName
        cell.reposioryNameLabel.text = model.name
        cell.reposioryDescriptionLabel.text = model.description
        cell.starCountLabel.text = "\(model.starCount)"
        cell.languageLabel.text = model.primaryLanguageName
        cell.languageColorView.backgroundColor = model.primaryLanguageColor?.hexToUiColor()
        cell.avatarImageView.tag = indexPath.item
        if let url = URL(string: model.ownerAvatarUrl) {
            imageDownloader.loadImage(from: url) { image, _ in
                if indexPath.item == cell.avatarImageView.tag {
                    cell.avatarImageView.image = image
                }
            }
        }
        return cell
    }
}

extension UICollectionViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
