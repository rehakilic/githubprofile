//
//  RepositoryBuilder.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol RepositoryBuilder {
    func build(models: [Repository],
               collectionLayoutType: CollectionLayoutType,
               title: String) -> RepositoryViewController
}

extension Dependencies: RepositoryBuilder {
    func build(models: [Repository],
               collectionLayoutType: CollectionLayoutType,
               title: String) -> RepositoryViewController {
        let presenter = RepositoryPresenter(model: RepositoryModel(title: title))
        let dataSource = RepositoryDataSource(models: models, imageDownloader: makeImageDownloader())
        let controller = RepositoryViewController(presenter: presenter,
                                                  dataSource: dataSource,
                                                  collectionLayoutType: collectionLayoutType)
        presenter.repositoryView = controller
        return controller
    }
}

