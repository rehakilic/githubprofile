//
//  ProfilePresenter.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol ProfilePresenterProtocol {
    func didLoad()
}

final class ProfilePresenter: ProfilePresenterProtocol {
    weak var profileView: ProfileViewProtocol?
    private let apolloNetworkController: ApolloNetworkController
    private let expirationController: ExpirationController
    
    init(apolloNetworkController: ApolloNetworkController,
         expirationController: ExpirationController) {
        self.apolloNetworkController = apolloNetworkController
        self.expirationController = expirationController
    }
    
    func didLoad() {
        profileView?.showLoading()
        if expirationController.isCacheExpired { apolloNetworkController.apollo.clearCache() }
        self.fetchProfile()
    }
}

private extension ProfilePresenter {
    func fetchProfile() {
        let query = FetchQuery(number_of_top_repositories: 10,
                               number_of_starred_repositories: 10,
                               number_of_pinned_repositories: 3)
        apolloNetworkController.apollo.fetch(query: query) { [weak self] result in
            self?.profileView?.removeLoading()
            switch result {
              case .success(let graphQLResult):
                guard let profile = graphQLResult.data?.viewer.createProfile() else { return }
                self?.profileView?.addContent(for: profile)
                self?.expirationController.saveCacheTime()
              case .failure(let error):
                self?.profileView?.errorOccured(error.localizedDescription)
              }
        }
    }
}
