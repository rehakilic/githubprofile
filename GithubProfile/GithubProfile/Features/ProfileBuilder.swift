//
//  ProfileBuilder.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol ProfileBuilder {
    func build() -> ProfileViewController
}

extension Dependencies: ProfileBuilder {
    func build() -> ProfileViewController {
        let presenter = ProfilePresenter(apolloNetworkController: makeApolloNetworkController(),
                                         expirationController: makeProfileExpirationController())
        let controller = ProfileViewController(presenter: presenter, factory: self)
        presenter.profileView = controller
        return controller
    }
}
