//
//  ProfileViewController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import UIKit


protocol ProfileViewProtocol: AnyObject {
    func addContent(for profile: Profile)
    func errorOccured(_ error: String)
    func showLoading()
    func removeLoading()
}

final class ProfileViewController: UIViewController {
    private(set) var profileView = ProfileView()
    private let presenter: ProfilePresenterProtocol
    typealias Factory = ProfileHeaderBuilder & RepositoryBuilder
    private let factory: Factory
    
    init(presenter: ProfilePresenterProtocol, factory: Factory) {
        self.presenter = presenter
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = profileView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefreshControl()
        presenter.didLoad()
    }
}

extension ProfileViewController: ProfileViewProtocol {
    func addContent(for profile: Profile) {
        removeChildren()
        addHeader(for: profile)
        addRepositories(profile.pinnedRepositories, collectionLayoutType: .vertical, title: "Pinned")
        addRepositories(profile.topRepositories, collectionLayoutType: .horizontal, title: "Top Repositories")
        addRepositories(profile.starredRepositories, collectionLayoutType: .horizontal, title: "Starred Repositories")
    }
    
    func errorOccured(_ error: String) {
        profileView.addErrorLabel(error: error)
    }
    
    func showLoading() {
        profileView.addLoading()
    }
    
    func removeLoading() {
        profileView.removeLoading()
    }
}

private extension ProfileViewController {
    func addRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControllerPulled(_:)), for: .valueChanged)
        profileView.scrollView.refreshControl = refreshControl
    }
    
    @objc
    func refreshControllerPulled(_ sender: UIRefreshControl) {        
        presenter.didLoad()
        sender.endRefreshing()
    }
}

private extension ProfileViewController {
    func addHeader(for profile: Profile) {
        let model = HeaderModel.create(from: profile)
        let controller: ProfileHeaderViewController = factory.build(model: model)
        add(controller, to: profileView.stackView)
    }
    
    func addRepositories(_ repositories: [Repository],
                         collectionLayoutType: CollectionLayoutType,
                         title: String) {
        let controller: RepositoryViewController = factory.build(models: repositories, collectionLayoutType: collectionLayoutType, title: title)
        add(controller, to: profileView.stackView)
        NSLayoutConstraint.activate([
            controller.view.heightAnchor.constraint(equalToConstant: collectionLayoutType.height)
        ])
    }
    
    func removeChildren() {
        children.forEach { $0.remove() }
        profileView.clearStackViewSubviews()
    }
}
