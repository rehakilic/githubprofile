//
//  ProfileView.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import UIKit

final class ProfileView: UIView {
    lazy var stackView = makeStackView()
    let scrollView = UIScrollView()
    private lazy var errorLabel = makeErrorLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupView()
        scrollView.alwaysBounceVertical = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addErrorLabel(error: String) {
        errorLabel.text = error
        stackView.addArrangedSubview(errorLabel)
    }
    
    func addLoading() {
        let indicatorView = UIActivityIndicatorView(style: .large)
        indicatorView.startAnimating()
        stackView.insertArrangedSubview(indicatorView, at: 0)
    }
    
    func removeLoading() {
        for subview in stackView.arrangedSubviews where subview is UIActivityIndicatorView {
            stackView.removeArrangedSubview(subview)
            subview.removeFromSuperview()
        }
    }
    
    func clearStackViewSubviews() {
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
        }
    }
}

private extension ProfileView {
    func setupView() {        
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        let scrollViewConstraints = [
            scrollView.topAnchor.constraint(equalTo:  safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor)
        ]
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        let stackViewConstraints = [
            stackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.frameLayoutGuide.widthAnchor),
        ]
        
        NSLayoutConstraint.activate(scrollViewConstraints)
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
    func makeStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = DimensionConstant.Spacing.large
        return stackView
    }
    
    func makeErrorLabel() -> UILabel {
        let label = UILabel()
        label.font = FontBook.bold.withStyle(of: .title1)
        label.textColor = .red
        return label
    }
}
