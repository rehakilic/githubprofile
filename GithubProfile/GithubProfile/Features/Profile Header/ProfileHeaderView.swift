//
//  ProfileHeaderView.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 23.12.2021.
//

import UIKit

final class ProfileHeaderView: UIView {
    lazy var avatarImageView = makeImageView()
    lazy var nameLabel = makeLabel(font: .bold, textStyle: .largeTitle)
    lazy var userNameLabel = makeLabel(font: .regular)
    lazy var mailLabel = makeLabel(font: .bold)
    lazy var followerLabel = makeLabel(font: .bold)
    lazy var followingLabel = makeLabel(font: .bold)
    lazy var followerTitleLabel = makeLabel(font: .regular)
    lazy var followingTitleLabel = makeLabel(font: .regular)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ProfileHeaderView {
    func setupView() {
        let nameStackView = UIStackView(arrangedSubviews: [nameLabel, userNameLabel])
        nameStackView.axis = .vertical
        nameStackView.spacing = DimensionConstant.Spacing.xsmall
        
        let imageStackView = UIStackView(arrangedSubviews: [avatarImageView, nameStackView])
        imageStackView.spacing = DimensionConstant.Spacing.xsmall
        imageStackView.alignment = .center
        
        let followerStackView = UIStackView(arrangedSubviews: [followerLabel, followerTitleLabel])
        followerStackView.spacing = DimensionConstant.Spacing.xsmall
        let followingStackView = UIStackView(arrangedSubviews: [followingLabel, followingTitleLabel])
        followingStackView.spacing = DimensionConstant.Spacing.xsmall
        let followStackView = UIStackView(arrangedSubviews: [followerStackView, followingStackView])
        followStackView.spacing = DimensionConstant.Spacing.medium
        
        let stackView = UIStackView(arrangedSubviews: [imageStackView, mailLabel, followStackView])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = DimensionConstant.Spacing.large
        
        addSubview(stackView)
        
        let imageSize: CGFloat = 88
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.layer.cornerRadius = imageSize / 2
        let avatarImageViewConstraints = [
            avatarImageView.widthAnchor.constraint(equalToConstant: imageSize),
            avatarImageView.heightAnchor.constraint(equalToConstant: imageSize)
        ]
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        let stackViewConstraints = [
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: DimensionConstant.Spacing.medium),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: DimensionConstant.Spacing.medium),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -DimensionConstant.Spacing.medium),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -DimensionConstant.Spacing.medium)
        ]
        
        NSLayoutConstraint.activate(avatarImageViewConstraints)
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
    func configure() {
        avatarImageView.clipsToBounds = true
        followerTitleLabel.text = "followers"
        followingTitleLabel.text = "following"
    }
}

private extension ProfileHeaderView {
    func makeLabel(font: FontBook, textStyle: UIFont.TextStyle = .callout) -> UILabel {
        let label = UILabel()
        label.font = font.withStyle(of: textStyle)
        label.textColor = .black
        return label
    }
    
    func makeImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
}
