//
//  ProfileHeaderViewController.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 23.12.2021.
//

import Foundation
import UIKit

protocol ProfileHeaderViewProtocol: AnyObject {
    func render(with model: HeaderModel)
    func setAvatarImage(_ image: UIImage?)
}

final class ProfileHeaderViewController: UIViewController {
    private(set) var profileHeaderView = ProfileHeaderView()
    private let presenter: ProfileHeaderPresenterProtocol
    
    init(presenter: ProfileHeaderPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = profileHeaderView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.didLoad()
    }
}

extension ProfileHeaderViewController: ProfileHeaderViewProtocol {
    func render(with model: HeaderModel) {
        profileHeaderView.nameLabel.text = model.name
        profileHeaderView.userNameLabel.text = model.login
        profileHeaderView.mailLabel.text = model.email
        profileHeaderView.followerLabel.text = "\(model.followerCount)"
        profileHeaderView.followingLabel.text = "\(model.followingCount)"
    }
    
    func setAvatarImage(_ image: UIImage?) {
        profileHeaderView.avatarImageView.image = image
    }
}
