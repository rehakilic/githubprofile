//
//  ProfileHeaderBuilder.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol ProfileHeaderBuilder {
    func build(model: HeaderModel) -> ProfileHeaderViewController
}

extension Dependencies: ProfileHeaderBuilder {
    func build(model: HeaderModel) -> ProfileHeaderViewController {
        let presenter = ProfileHeaderPresenter(model: model, imageDownloader: makeImageDownloader())
        let controller = ProfileHeaderViewController(presenter: presenter)
        presenter.profileHeaderView = controller
        return controller
    }
}
