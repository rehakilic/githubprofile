//
//  ProfileHeaderPresenter.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation

protocol ProfileHeaderPresenterProtocol {
    func didLoad()
}

final class ProfileHeaderPresenter: ProfileHeaderPresenterProtocol {
    weak var profileHeaderView: ProfileHeaderViewProtocol?
    private let model: HeaderModel
    private let imageDownloader: ImageDownloader
    
    init(model: HeaderModel, imageDownloader: ImageDownloader) {
        self.model = model
        self.imageDownloader = imageDownloader
    }
    
    func didLoad() {
        profileHeaderView?.render(with: model)
        guard let url = URL(string: model.avatarUrl) else { return }
        imageDownloader.loadImage(from: url) { [weak self] image, _ in
            self?.profileHeaderView?.setAvatarImage(image)
        }
    }
}

struct HeaderModel {
    let name: String?
    let login: String
    let avatarUrl: String
    let email: String
    let followerCount: Int
    let followingCount: Int
    
    static func create(from profile: Profile) -> HeaderModel {
        return HeaderModel(name: profile.name,
                     login: profile.login,
                     avatarUrl: profile.avatarUrl,
                     email: profile.email,
                     followerCount: profile.followerCount,
                     followingCount: profile.followingCount)
    }
}
