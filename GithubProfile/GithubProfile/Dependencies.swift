//
//  Dependencies.swift
//  GithubProfile
//
//  Created by Reha Kılıç on 24.12.2021.
//

import Foundation
import Apollo
import ApolloSQLite

struct Dependencies {
    let userDefaultsController: UserDefaultsController

    internal init(userDefaultsController: UserDefaultsController = .init()) {
        self.userDefaultsController = userDefaultsController
    }
}

protocol DependencyFactory {
    func makeImageDownloader() -> ImageDownloader
    func makeProfileExpirationController() -> ProfileExpirationController
    func makeApolloNetworkController() -> ApolloNetworkController
}

extension Dependencies: DependencyFactory {
    func makeImageDownloader() -> ImageDownloader {
        return DefaultImageDownloader(session: URLSession.shared, imageCacher: ImageCacher())
    }
    
    func makeProfileExpirationController() -> ProfileExpirationController {
        return ProfileExpirationController(userDefaultsController: userDefaultsController)
    }
    
    func makeApolloNetworkController() -> ApolloNetworkController {
        let documentsPath = NSSearchPathForDirectoriesInDomains(
            .documentDirectory,
            .userDomainMask,
            true).first!
        let documentsURL = URL(fileURLWithPath: documentsPath)
        let sqliteFileURL = documentsURL.appendingPathComponent("githubprofile_apollo_db.sqlite")
        
        // 2. Use that file URL to instantiate the SQLite cache:
        guard let cache = try? SQLiteNormalizedCache(fileURL: sqliteFileURL) else {
            preconditionFailure("Database could not be created")
        }
        let store = ApolloStore(cache: cache)
        let client = URLSessionClient()
        let provider = NetworkInterceptorProvider(store: store, client: client)
        let url = URL(string: "https://api.github.com/graphql")!
        let networkTransport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                            endpointURL: url)
        return ApolloNetworkController(networkTransport: networkTransport, store: store)
    }
}
